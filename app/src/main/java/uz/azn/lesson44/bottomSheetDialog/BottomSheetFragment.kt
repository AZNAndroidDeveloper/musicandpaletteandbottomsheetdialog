package uz.azn.lesson44.bottomSheetDialog

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.Toast
import com.google.android.material.bottomsheet.BottomSheetBehavior
import uz.azn.lesson44.R
import uz.azn.lesson44.databinding.FragmentBottomSheetBinding

class BottomSheetFragment : Fragment(R.layout.fragment_bottom_sheet) {
    private lateinit var binding: FragmentBottomSheetBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentBottomSheetBinding.bind(view)

        val bottomSheetBehavior = BottomSheetBehavior.from(binding.bottomSheet)
//        bottomSheetBehavior.setBottomSheetCallback(object :BottomSheetBehavior.BottomSheetCallback(){
//
//            // surilatyotganda
//            override fun onSlide(bottomSheet: View, slideOffset: Float) {
//                Toast.makeText(requireContext(), "Sliding", Toast.LENGTH_SHORT).show()
//            }
//
//            override fun onStateChanged(bottomSheet: View, newState: Int) {
//                when(newState){
//                    // qolni bosib surayotgan holati
//                    BottomSheetBehavior.STATE_DRAGGING-> Toast.makeText(requireContext(), "Dragin", Toast.LENGTH_SHORT)
//                        .show()
//                    //kengaygan holati
//                    BottomSheetBehavior.STATE_EXPANDED-> Toast.makeText(requireContext(), "Expanded", Toast.LENGTH_SHORT)
//                        .show()
//                    // qaytib yopilgan holati
//                    BottomSheetBehavior.STATE_COLLAPSED-> Toast.makeText(
//                        requireContext(),
//                        "Collapsed",
//                        Toast.LENGTH_SHORT
//                    )
//                        .show()
//                }
//            }
//
//        })
        // qanchadir razmerda ochilishi
//bottomSheetBehavior.isFitToContents = false
//        bottomSheetBehavior.halfExpandedRatio = 0.6f

        // holati ochilishi va yopilishi
        with(binding) {
            btnSheet.setOnClickListener {
                if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_COLLAPSED) {
                    val metrics = resources.displayMetrics
                    bottomSheetBehavior.peekHeight = metrics.heightPixels/2
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_HALF_EXPANDED
                } else {
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                }
            }
        }
    }
}