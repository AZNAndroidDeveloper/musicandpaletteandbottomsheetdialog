package uz.azn.lesson44.intro

import android.app.Activity
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.palette.graphics.Palette
import com.squareup.picasso.Picasso
import uz.azn.lesson44.R
import uz.azn.lesson44.databinding.FragmentIntroBinding
import java.io.FileNotFoundException

class IntroFragment : Fragment(R.layout.fragment_intro) {
    private var imageUri: Uri? = null
    lateinit var vibrantSwatch: Palette.Swatch
    private val PICK_IMAGE_REQUEST_CODE = 123
    private lateinit var binding: FragmentIntroBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentIntroBinding.bind(view)
        with(binding) {
            btnChoose.apply {
                setOnClickListener {

                    openChooseImageDialog()
                }
            }
        }

    }

    fun openChooseImageDialog() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select an image"), PICK_IMAGE_REQUEST_CODE)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            imageUri = data.data
            Picasso.get().load(imageUri).fit().placeholder(R.drawable.ic_image).centerCrop().into(binding.imageView)
            try {
                val inputStream = activity!!.contentResolver.openInputStream(data.data!!)
                val bitmap = BitmapFactory.decodeStream(inputStream)
                // rang olish
                Palette.from(bitmap).generate {
                    vibrantSwatch = it?.vibrantSwatch!!
                    with(binding) {
                        rootLayout.setBackgroundColor(vibrantSwatch.rgb)
//                        toolbar.setBackgroundColor(vibrantSwatch.titleTextColor)
                        textView.setTextColor(vibrantSwatch.bodyTextColor)
                        imageView.borderColor = vibrantSwatch.bodyTextColor
                        btnChoose.setBackgroundColor(vibrantSwatch.bodyTextColor)
                        btnChoose.setTextColor(vibrantSwatch.rgb)
                    }
                }

            } catch (ex: FileNotFoundException) {
                ex.printStackTrace()
            }
        }

    }

}