package uz.azn.lesson44.badge

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class AdapterPager (val fragmentManager: FragmentManager):FragmentPagerAdapter(fragmentManager){
    val fragmentList = mutableListOf<Fragment>()
    val titleList = mutableListOf<String>()
    override fun getItem(position: Int): Fragment {
return fragmentList[position]
    }

    override fun getCount(): Int  = fragmentList.size
    override fun getPageTitle(position: Int): CharSequence? {
        return titleList[position]
    }

    fun setFragment(fragment:Fragment, title:String){
        fragmentList.add(fragment)
        titleList.add(title)
    }
}
