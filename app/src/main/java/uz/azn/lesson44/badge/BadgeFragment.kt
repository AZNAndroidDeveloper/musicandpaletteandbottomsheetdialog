package uz.azn.lesson44.badge

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.badge.BadgeDrawable
import uz.azn.lesson44.R
import uz.azn.lesson44.databinding.FragmentBadgeBinding
import uz.azn.lesson44.intro.IntroFragment
import uz.azn.lesson44.music.MusicFragment

class BadgeFragment : Fragment(R.layout.fragment_badge) {

    private lateinit var binding: FragmentBadgeBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    binding = FragmentBadgeBinding.bind(view)
        initAdapter()
    }


    private fun initAdapter(){
        val adapter = AdapterPager(fragmentManager!!)
        adapter.setFragment(IntroFragment(),"Palette")
        adapter.setFragment(MusicFragment(),"Music")
        adapter.setFragment(MusicFragment(),"Tab3")
        with(binding){
            viewPager.adapter = adapter
            tableLayout.apply {
                setupWithViewPager(viewPager)
            }
            val badgeWithNumber:BadgeDrawable =tableLayout.getTabAt(1)!!.orCreateBadge
            badgeWithNumber.isVisible = true
            badgeWithNumber.number = 5
            val roundBadge:BadgeDrawable =tableLayout.getTabAt(0)!!.orCreateBadge
            roundBadge.isVisible = true
            roundBadge.number = 60
            val badgeMoreWithNumber:BadgeDrawable =tableLayout.getTabAt(2)!!.orCreateBadge
            badgeMoreWithNumber.isVisible = true
            badgeMoreWithNumber.number = +100



        }

    }

}