package uz.azn.lesson44.music

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import uz.azn.lesson44.R
import uz.azn.lesson44.databinding.FragmentMusicBinding

class MusicFragment : Fragment(R.layout.fragment_music) {
    private lateinit var binding:FragmentMusicBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMusicBinding.bind(view)
        with(binding){
            start.apply {
                setOnClickListener {
                    val intent = Intent(requireContext(), MyService::class.java)
                    activity!!.startService(intent)
                }

            }
            stop.apply {
                setOnClickListener {
                    val intent = Intent(requireContext(), MyService::class.java)
                    activity!!.stopService(intent)
                }
            }
        }
    }
}