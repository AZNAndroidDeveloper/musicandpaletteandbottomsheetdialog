package uz.azn.lesson44.music

import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import android.os.IBinder
import uz.azn.lesson44.R
import java.security.Provider

class MyService() : Service() {
    private lateinit var mediaPlayer: MediaPlayer
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        mediaPlayer = MediaPlayer.create(this, R.raw.janon_bolaman)
        super.onCreate()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        mediaPlayer.start()
        return START_STICKY
    }

    override fun onDestroy() {
        if (mediaPlayer.isPlaying) {
            mediaPlayer.apply {
                stop()
                reset()
                release()

            }


        }
        super.onDestroy()
    }

}