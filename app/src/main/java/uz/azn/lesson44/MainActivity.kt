package uz.azn.lesson44

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import uz.azn.lesson44.badge.BadgeFragment
import uz.azn.lesson44.bottomSheetDialog.BottomSheetFragment
import uz.azn.lesson44.intro.IntroFragment
import uz.azn.lesson44.music.MusicFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction().replace(R.id.frame,
            BadgeFragment()
        ).commit()
    }
}